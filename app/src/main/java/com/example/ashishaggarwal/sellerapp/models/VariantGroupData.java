package com.example.ashishaggarwal.sellerapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VariantGroupData {

    @SerializedName("group_id")
    private String groupId;

    private String name;

    private List<VariationDetails> variations;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<VariationDetails> getVariations() {
        return variations;
    }

    public void setVariations(List<VariationDetails> variations) {
        this.variations = variations;
    }
}
