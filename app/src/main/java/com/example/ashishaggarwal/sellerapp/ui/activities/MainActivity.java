package com.example.ashishaggarwal.sellerapp.ui.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.ashishaggarwal.sellerapp.commons.Utils;
import com.example.ashishaggarwal.sellerapp.models.VariationDetails;
import com.example.ashishaggarwal.sellerapp.ui.viewmodels.MainViewModel;
import com.example.ashishaggarwal.sellerapp.R;
import com.example.ashishaggarwal.sellerapp.models.VariantsData;
import com.example.ashishaggarwal.sellerapp.ui.adapters.GroupsDataAdapter;

public class MainActivity extends AppCompatActivity {

    private MainViewModel viewModel;

    private RecyclerView recyclerView;

    private GroupsDataAdapter adapter;

    private TextView loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        createRecyclerView();
        loading = findViewById(R.id.loading);
        viewModel.getLiveData().observe(this, dataObserver());
    }

    private Observer<VariantsData> dataObserver() {
        return new Observer<VariantsData>() {
            @Override
            public void onChanged(@Nullable VariantsData o) {
                if (o == null) {
                    loading.setText(getString(R.string.some_error_occured));
                    return;
                }
                loading.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                adapter.setVariantGroupData(o.getVariantGroups());
            }
        };
    }

    private void createRecyclerView() {
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new GroupsDataAdapter(this, viewModel);
        recyclerView.setAdapter(adapter);
    }
}
