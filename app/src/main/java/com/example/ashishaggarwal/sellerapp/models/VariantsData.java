package com.example.ashishaggarwal.sellerapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VariantsData {

    @SerializedName("variant_groups")
    private List<VariantGroupData> variantGroups;

    @SerializedName("exclude_list")
    private List<List<ExcludeData>> excludeList;

    public List<VariantGroupData> getVariantGroups() {
        return variantGroups;
    }

    public void setVariantGroups(List<VariantGroupData> variantGroups) {
        this.variantGroups = variantGroups;
    }

    public List<List<ExcludeData>> getExcludeList() {
        return excludeList;
    }

    public void setExcludeList(List<List<ExcludeData>> excludeList) {
        this.excludeList = excludeList;
    }
}
