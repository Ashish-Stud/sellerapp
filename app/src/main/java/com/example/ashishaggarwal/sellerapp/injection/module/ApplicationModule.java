package com.example.ashishaggarwal.sellerapp.injection.module;

import android.app.Application;
import android.content.Context;

import com.example.ashishaggarwal.sellerapp.commons.ApiService;
import com.example.ashishaggarwal.sellerapp.injection.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Provide application-level dependencies.
 */
@Module
public class ApplicationModule {

    protected final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    ApiService provideApiService() {
        return new ApiService.Creator().newApiService();
    }



}
