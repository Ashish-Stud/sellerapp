package com.example.ashishaggarwal.sellerapp.models;

public class VariantResponse {

    private VariantsData variants;

    public VariantsData getVariants() {
        return variants;
    }

    public void setVariants(VariantsData variants) {
        this.variants = variants;
    }
}
