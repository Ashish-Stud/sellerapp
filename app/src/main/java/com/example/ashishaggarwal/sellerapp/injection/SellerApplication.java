package com.example.ashishaggarwal.sellerapp.injection;

import android.app.Application;

import com.example.ashishaggarwal.sellerapp.injection.component.ApplicationComponent;
import com.example.ashishaggarwal.sellerapp.injection.component.DaggerApplicationComponent;
import com.example.ashishaggarwal.sellerapp.injection.module.ApplicationModule;


public class SellerApplication extends Application {

    ApplicationComponent mApplicationComponent;

    private static SellerApplication _instance;

    @Override
    public void onCreate() {
        super.onCreate();
        _instance = this;
    }

    public static SellerApplication getInstance() {
        return _instance;
    }

    public ApplicationComponent getComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return mApplicationComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }
}
