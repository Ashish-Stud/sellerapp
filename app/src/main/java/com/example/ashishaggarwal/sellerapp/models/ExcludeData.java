package com.example.ashishaggarwal.sellerapp.models;

import com.google.gson.annotations.SerializedName;

public class ExcludeData {

    @SerializedName("group_id")
    private String groupId;

    @SerializedName("variation_id")
    private String variationId;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getVariationId() {
        return variationId;
    }

    public void setVariationId(String variationId) {
        this.variationId = variationId;
    }
}
