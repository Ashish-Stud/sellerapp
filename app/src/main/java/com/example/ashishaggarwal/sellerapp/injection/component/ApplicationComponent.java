package com.example.ashishaggarwal.sellerapp.injection.component;

import android.app.Application;
import android.content.Context;

import com.example.ashishaggarwal.sellerapp.commons.ApiService;
import com.example.ashishaggarwal.sellerapp.ui.viewmodels.MainViewModel;
import com.example.ashishaggarwal.sellerapp.injection.ApplicationContext;
import com.example.ashishaggarwal.sellerapp.injection.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    @ApplicationContext
    Context context();

    Application application();

    ApiService getApiService();

    void inject(MainViewModel viewModel);
}
