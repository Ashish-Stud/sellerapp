package com.example.ashishaggarwal.sellerapp.ui.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.ashishaggarwal.sellerapp.R;
import com.example.ashishaggarwal.sellerapp.commons.Utils;
import com.example.ashishaggarwal.sellerapp.models.VariantGroupData;
import com.example.ashishaggarwal.sellerapp.models.VariationDetails;
import com.example.ashishaggarwal.sellerapp.ui.viewmodels.MainViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupsDataAdapter extends RecyclerView.Adapter<GroupsDataAdapter.GroupDataViewHolder> {

    private final Activity context;

    private final MainViewModel viewModel;

    private List<VariantGroupData> variantGroupData;


    private Map<String, List<String>> excludeMap;

    private Map<String, String> selectedMap;
    
    private Map<String, RadioButton> mapRadioButton = new HashMap<>();

    private Map<String, Map<String, VariationDetails>> mapVariantDetails = new HashMap<>();

    public GroupsDataAdapter(Activity context, MainViewModel viewModel) {
        this.context = context;
        this.viewModel = viewModel;
        variantGroupData = new ArrayList<>();
    }

    public void setVariantGroupData(List<VariantGroupData> variantGroupData) {
        this.variantGroupData = variantGroupData;
        this.excludeMap = viewModel.getExcludeMap();
        this.selectedMap = viewModel.getSelectedMap();
        notifyDataSetChanged();
    }

    public List<VariantGroupData> getVariantGroupData() {
        return variantGroupData;
    }

    @Override
    public GroupDataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.group_item, parent, false);
        return new GroupDataViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final GroupDataViewHolder holder, int position) {
        VariantGroupData variantData = variantGroupData.get(position);
        holder.groupText.setText(variantData.getName());
        feedRadioGroup(variantData, holder.radioGroup);
    }

    private void feedRadioGroup(VariantGroupData variantData, RadioGroup radioGroup) {
        radioGroup.removeAllViews();
        radioGroup.setTag(variantData.getGroupId());
        for (VariationDetails details : variantData.getVariations()) {
            RadioButton button = getRadioButton(radioGroup.getId(), variantData.getGroupId(), details);
            radioGroup.addView(button);
            button.setChecked(details.isChecked());
        }
    }

    public RadioButton getRadioButton(int id, String groupId, VariationDetails details) {
        String key = Utils.getKey(groupId, details.getId());
        RadioButton button = mapRadioButton.get(key);
        if (button != null) {
            return button;
        }
        button = new RadioButton(context);
        button.setText(details.getName() + " " + context.getString(R.string.in_stock) + ":" + getStockText(details.getInStock()) + " " + context.getString(R.string.price) + ":" + details.getPrice());
        button.setTag(key);
        button.setEnabled(details.isEnabled());
        button.setOnClickListener(onClickListener);
        mapRadioButton.put(Utils.getKey(groupId, details.getId()), button);
        putdataInMapvariantDetails(groupId, details.getId(), details);
        return button;
    }

    private void putdataInMapvariantDetails(String groupId, String id, VariationDetails details) {
        if (mapVariantDetails.get(groupId) == null) {
            mapVariantDetails.put(groupId, new HashMap<String, VariationDetails>());
        }
        mapVariantDetails.get(groupId).put(id, details);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String tag = (String) view.getTag();
            selectedMap.put(getGroupId(tag), tag);
            setCheckedState(getGroupId(tag), getVariantId(tag));
            setRadioButtonStates();

        }
    };

    private String getVariantId(String tag) {
        return tag.split(":")[1];
    }

    private String getGroupId(String tag) {
        return tag.split(":")[0];
    }

    private void setCheckedState(String groupId, String variantId) {
        Map<String, VariationDetails> map = mapVariantDetails.get(groupId);
        for (String key : map.keySet()) {
            if (map.get(key).getId().equals(variantId)) {
                map.get(key).setChecked(true);
            } else {
                map.get(key).setChecked(false);
            }
        }
    }

    private void setRadioButtonStates() {
        for (RadioButton btn : mapRadioButton.values()) {
            btn.setEnabled(true);
        }

        for (Map<String, VariationDetails> map : mapVariantDetails.values()) {
            for (VariationDetails details : map.values()) {
                details.setEnabled(true);
            }
        }

        for (String key : selectedMap.values()) {
            if (excludeMap.get(key) != null) {
                for (String str : excludeMap.get(key)) {
                    mapRadioButton.get(str).setEnabled(false);
                    mapVariantDetails.get(getGroupId(str)).get(getVariantId(str)).setEnabled(false);
                }
            }
        }
    }

    String getStockText(int inStock) {
        if (inStock == 1)
            return context.getString(R.string.yes);
        return context.getString(R.string.no);
    }

    @Override
    public int getItemCount() {
        return variantGroupData.size();
    }


    public class GroupDataViewHolder extends RecyclerView.ViewHolder {

        private final TextView groupText;

        private final RadioGroup radioGroup;

        public GroupDataViewHolder(View itemView) {
            super(itemView);
            groupText = itemView.findViewById(R.id.groupText);
            radioGroup = itemView.findViewById(R.id.radioGrp);
        }
    }
}
