package com.example.ashishaggarwal.sellerapp.models;

import com.google.gson.annotations.SerializedName;

public class VariationDetails {

    private String name;

    private int price;

    @SerializedName("default")
    private int defaultData;

    private String id;

    private int inStock;

    private int isVeg;

    private boolean isEnabled = true;

    private boolean isChecked = false;

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getDefaultData() {
        return defaultData;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getInStock() {
        return inStock;
    }

    public void setInStock(int inStock) {
        this.inStock = inStock;
    }

    public int getIsVeg() {
        return isVeg;
    }

    public void setIsVeg(int isVeg) {
        this.isVeg = isVeg;
    }

    public void setDefaultData(int defaultData) {
        this.defaultData = defaultData;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }
}
