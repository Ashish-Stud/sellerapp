package com.example.ashishaggarwal.sellerapp.commons;

import com.example.ashishaggarwal.sellerapp.models.VariantResponse;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public interface ApiService {

    @GET("bins/3b0u2")
    Observable<VariantResponse> getVariantsResponse();

    class Creator {

        public static ApiService newApiService() {
            Retrofit retrofit = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(AppConstants.BASE_URL)
                    .client(new OkHttpClient())
                    .build();
            return retrofit.create(ApiService.class);
        }
    }
}
