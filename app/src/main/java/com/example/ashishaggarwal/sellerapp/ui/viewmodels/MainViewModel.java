package com.example.ashishaggarwal.sellerapp.ui.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.example.ashishaggarwal.sellerapp.commons.Utils;
import com.example.ashishaggarwal.sellerapp.injection.SellerApplication;
import com.example.ashishaggarwal.sellerapp.commons.ApiService;
import com.example.ashishaggarwal.sellerapp.models.ExcludeData;
import com.example.ashishaggarwal.sellerapp.models.VariantResponse;
import com.example.ashishaggarwal.sellerapp.models.VariantsData;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel extends ViewModel {

    private Disposable disposableObserver;

    @Inject
    public ApiService apiService;

    private Map<String, List<String>> excludeMap = new HashMap<>();

    private Map<String, String> selectedMap = new HashMap<>();

    MutableLiveData<VariantsData> liveData = new MutableLiveData<>();

    public MainViewModel() {
        SellerApplication.getInstance().getComponent().inject(this);
        disposableObserver = apiService.getVariantsResponse().subscribeOn(Schedulers.io()).map(new Function<VariantResponse, VariantsData>() {
            @Override
            public VariantsData apply(VariantResponse variantResponse) throws Exception {
                return variantResponse.getVariants();
            }
        }).observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<VariantsData>() {
            @Override
            public void onNext(VariantsData o) {
                feedExcludeMap(o.getExcludeList());
                liveData.setValue(o);
            }

            @Override
            public void onError(Throwable e) {
                liveData.setValue(null);
                Log.d("ashish1234", e.toString());
            }

            @Override
            public void onComplete() {

            }
        });
    }


    private void feedExcludeMap(List<List<ExcludeData>> excludeList) {
        for (List<ExcludeData> currentList : excludeList) {
            for (ExcludeData excludeData : currentList) {
                List<ExcludeData> copyCurrentList = new ArrayList<>(currentList);
                copyCurrentList.remove(excludeData);
                String key = Utils.getKey(excludeData.getGroupId(), excludeData.getVariationId());
                if (!excludeMap.containsKey(key)) {
                    excludeMap.put(key, new ArrayList<String>());
                }
                List<String> list = excludeMap.get(key);
                for (ExcludeData data : copyCurrentList) {
                    String itemKey = Utils.getKey(data.getGroupId(), data.getVariationId());
                    if (!list.contains(itemKey))
                        list.add(itemKey);
                }
            }
        }
    }

    public MutableLiveData<VariantsData> getLiveData() {
        return liveData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposableObserver != null)
            disposableObserver.dispose();
    }

    public Map<String, List<String>> getExcludeMap() {
        return excludeMap;
    }

    public Map<String, String> getSelectedMap() {
        return selectedMap;
    }
}
